/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package csvrec

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/srosencrantz/csv"
	"bitbucket.org/srosencrantz/errcat"
)

// RecordMap contains lists of all the records read or to be written, mapped to the record labels.
type RecordMap map[string][][]string

// ReadFile reads a csv record file and returns a map of the records
func ReadFile(name string) (recordMap RecordMap, err error) {
	var f *os.File
	if f, err = os.Open(name); err != nil {
		return nil, err
	}
	defer f.Close()

	rcsv := csv.NewReader(f)
	rcsv.Comma = ','
	rcsv.Comment = '/'
	rcsv.FieldsPerRecord = -1
	rcsv.TrimLeadingSpace = true

	var records [][]string
	records, err = rcsv.ReadAll()
	if err != nil {
		return nil, err
	}

	TrimSpace(records)
	recordMap = make(RecordMap)

	for _, r := range records {
		recordMap[r[0]] = append(recordMap[r[0]], r)
	}

	return recordMap, nil
}

// ReadFileAndComments reads a csv record file and its comments (lines starting with //) and
//  returns a map of the records.
func ReadFileAndComments(name string) (recordMap RecordMap, err error) {
	var f *os.File
	if f, err = os.Open(name); err != nil {
		return nil, err
	}
	defer f.Close()

	rcsv := csv.NewReader(f)
	rcsv.Comma = ','
	rcsv.FieldsPerRecord = -1

	var records [][]string
	records, err = rcsv.ReadAll()
	if err != nil {
		return nil, err
	}

	TrimSpace(records)
	recordMap = make(RecordMap)

	for _, r := range records {
		recordMap[r[0]] = append(recordMap[r[0]], r)
	}

	return recordMap, nil
}

// The following are helper functions

// TrimSpace trims the spaces from all the strings in a [][]string
func TrimSpace(records [][]string) {
	for i := range records {
		for j := range records[i] {
			records[i][j] = strings.TrimSpace(records[i][j])
		}
	}
}

// TrimList trims the spaces from a list of strings
func TrimList(strs []string) (new []string) {
	new = make([]string, len(strs))
	for i := range strs {
		new[i] = strings.TrimSpace(strs[i])
	}
	return new
}

// ReadInt converts an string to an int and concatenates any errors occrued
func ReadInt(field string, finalErr *error) (num int) {
	num64, err := strconv.ParseInt(strings.TrimSpace(field), 10, 0)
	*finalErr = errcat.AppendErr(*finalErr, err)
	return int(num64)
}

// ReadIntList reads multiple int records
func ReadIntList(fields []string, finalErr *error) (val []int) {
	val = make([]int, len(fields))
	for i, field := range fields {
		val[i] = ReadInt(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadIntList failed")
			return val
		}
	}
	return val
}

// ReadFlt converts a string to a float64 and concatenates any errors occrued
func ReadFlt(field string, finalErr *error) (num float64) {
	var err error
	num, err = strconv.ParseFloat(strings.TrimSpace(field), 64)
	*finalErr = errcat.AppendErr(*finalErr, err)
	return num
}

// ReadFltList reads multiple flt records
func ReadFltList(fields []string, finalErr *error) (val []float64) {
	val = make([]float64, len(fields))
	for i, field := range fields {
		val[i] = ReadFlt(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadFltList failed")
			return val
		}
	}
	return val
}

// ReadBool converts a string ("1", "0", "true", "false") to a bool and concatenates any errors occrued
func ReadBool(field string, finalErr *error) (val bool) {
	switch strings.ToLower(strings.TrimSpace(field)) {
	case "0", "false":
		return false
	case "1", "true":
		return true
	}
	*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("Expected a boolean value (0, false, 1, or true) but found: %v", field))
	return false
}

// ReadBoolList reads multiple bool records
func ReadBoolList(fields []string, finalErr *error) (val []bool) {
	val = make([]bool, len(fields))
	for i, field := range fields {
		val[i] = ReadBool(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadBoolList failed")
			return val
		}
	}
	return val
}

// ReadStr converts a string to a trimmed string and concatenates any errors occrued
func ReadStr(field string, finalErr *error) (val string) {
	val = strings.TrimSpace(field)
	return val
}

// ReadStrList reads multiple str records
func ReadStrList(fields []string, finalErr *error) (val []string) {
	val = make([]string, len(fields))
	for i, field := range fields {
		val[i] = ReadStr(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadStrList failed")
			return val
		}
	}
	return val
}

// ReadIntPair converts a colon seperated string of up to two ints to a [2]int array, if there is a single int then the array will have two of the same value.
func ReadIntPair(field string, finalErr *error) (vals [2]int) {
	strs := strings.Split(field, ":")
	if (len(strs) != 1) && (len(strs) != 2) {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("ReadIntPair field has %d colon seperated subfields expected either 1 or 2, %v", len(strs), field))
		return vals
	}
	if len(strs) == 2 {
		return [2]int{ReadInt(strs[0], finalErr), ReadInt(strs[1], finalErr)}
	}
	return [2]int{ReadInt(strs[0], finalErr), ReadInt(strs[0], finalErr)}
}

// ReadIntPairList reads multiple intPair records
func ReadIntPairList(fields []string, finalErr *error) (val [][2]int) {
	val = make([][2]int, len(fields))
	for i, field := range fields {
		val[i] = ReadIntPair(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadIntPairList failed")
			return val
		}
	}
	return val
}

// ReadIntRange converts a colon seperated string of up to two ints to a [2]int array, if there is a single int then the array will have two of the same value.
func ReadIntRange(field string, finalErr *error) (vals [3]int) {
	strs := strings.Split(field, ":")
	if (len(strs) != 1) && (len(strs) != 3) {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("ReadIntRange field has %d colon seperated subfields expected either 1 or 3, %v", len(strs), field))
		return vals
	}
	if len(strs) == 3 {
		return [3]int{ReadInt(strs[0], finalErr), ReadInt(strs[1], finalErr), ReadInt(strs[2], finalErr)}
	}
	return [3]int{ReadInt(strs[0], finalErr), ReadInt(strs[0], finalErr), ReadInt(strs[0], finalErr)}
}

// ReadIntRangeList reads multiple intRange records
func ReadIntRangeList(fields []string, finalErr *error) (val [][3]int) {
	val = make([][3]int, len(fields))
	for i, field := range fields {
		val[i] = ReadIntRange(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadIntRangeList failed")
			return val
		}
	}
	return val
}

// ReadInt2Vec converts a colon seperated string of exactly two ints to a [2]int array.
func ReadInt2Vec(field string, finalErr *error) (vals [2]int) {
	strs := strings.Split(field, ":")
	if len(strs) != 2 {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("ReadInt2Vec field has %d colon seperated subfields expected  2, %v", len(strs), field))
		return vals
	}
	return [2]int{ReadInt(strs[0], finalErr), ReadInt(strs[1], finalErr)}
}

// ReadInt2VecList reads multiple int2Vec records
func ReadInt2VecList(fields []string, finalErr *error) (val [][2]int) {
	val = make([][2]int, len(fields))
	for i, field := range fields {
		val[i] = ReadInt2Vec(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadInt2VecList failed")
			return val
		}
	}
	return val
}

// ReadInt3Vec converts a colon seperated string of exactly 3 ints to a [3]int array.
func ReadInt3Vec(field string, finalErr *error) (vals [3]int) {
	strs := strings.Split(field, ":")
	if len(strs) != 3 {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("ReadInt3Vec field has %d colon seperated subfields expected 3, %v", len(strs), field))
		return vals
	}
	return [3]int{ReadInt(strs[0], finalErr), ReadInt(strs[1], finalErr), ReadInt(strs[2], finalErr)}
}

// ReadInt3VecList reads multiple int3Vec records
func ReadInt3VecList(fields []string, finalErr *error) (val [][3]int) {
	val = make([][3]int, len(fields))
	for i, field := range fields {
		val[i] = ReadInt3Vec(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadInt3VecList failed")
			return val
		}
	}
	return val
}

// ReadFltPair converts a colon seperated string of up to two floats to a [2]float64 array, if there is a single float then the array will have two of the same value.
func ReadFltPair(field string, finalErr *error) (vals [2]float64) {
	strs := strings.Split(field, ":")
	if (len(strs) != 1) && (len(strs) != 2) {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("ReadFltPair field has %d colon seperated subfields expected either 1 or 2, %v", len(strs), field))
		return vals
	}
	if len(strs) == 2 {
		return [2]float64{ReadFlt(strs[0], finalErr), ReadFlt(strs[1], finalErr)}
	}
	singleVal := ReadFlt(field, finalErr)
	return [2]float64{singleVal, singleVal}
}

// ReadFltPairList reads multiple fltPair records
func ReadFltPairList(fields []string, finalErr *error) (val [][2]float64) {
	val = make([][2]float64, len(fields))
	for i, field := range fields {
		val[i] = ReadFltPair(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadFltPairList failed")
			return val
		}
	}
	return val
}

// ReadFltRange converts a colon seperated string of 1 (e.g. 1.23) or 3 floats (e.g. 1.23:4.56:7.89)  to a [3]float64 array, if there is a single float then the array will have three of the same value.
func ReadFltRange(field string, finalErr *error) (vals [3]float64) {
	strs := strings.Split(field, ":")
	if (len(strs) != 1) && (len(strs) != 3) {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("ReadFltRange field has %d colon seperated subfields expected either 1 or 3, %v", len(strs), field))
		return vals
	}
	if len(strs) == 3 {
		return [3]float64{ReadFlt(strs[0], finalErr), ReadFlt(strs[1], finalErr), ReadFlt(strs[2], finalErr)}
	}
	return [3]float64{ReadFlt(strs[0], finalErr), ReadFlt(strs[0], finalErr), ReadFlt(strs[0], finalErr)}
}

// ReadFltRangeList reads multiple fltRange records
func ReadFltRangeList(fields []string, finalErr *error) (val [][3]float64) {
	val = make([][3]float64, len(fields))
	for i, field := range fields {
		val[i] = ReadFltRange(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadFltRangeList failed")
			return val
		}
	}
	return val
}

// ReadFlt2Vec converts a colon seperated string of exactly two floats to a [2]float64 array.
func ReadFlt2Vec(field string, finalErr *error) (vals [2]float64) {
	strs := strings.Split(field, ":")
	if len(strs) != 2 {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("ReadFlt2Vec field has %d colon seperated subfields expected 2, %v", len(strs), field))
		return vals
	}
	return [2]float64{ReadFlt(strs[0], finalErr), ReadFlt(strs[1], finalErr)}
}

// ReadFlt2VecList reads multiple flt2Vec records
func ReadFlt2VecList(fields []string, finalErr *error) (val [][2]float64) {
	val = make([][2]float64, len(fields))
	for i, field := range fields {
		val[i] = ReadFlt2Vec(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadFlt2VecList failed")
			return val
		}
	}
	return val
}

// ReadFlt3Vec converts a colon seperated string of exactly 3 floats (e.g. 1.23:4.56:7.89)  to a [3]float64 array.
func ReadFlt3Vec(field string, finalErr *error) (vals [3]float64) {
	strs := strings.Split(field, ":")
	if len(strs) != 3 {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("ReadFlt3Vec field has %d colon seperated subfields expected 3, %v", len(strs), field))
		return vals
	}
	return [3]float64{ReadFlt(strs[0], finalErr), ReadFlt(strs[1], finalErr), ReadFlt(strs[2], finalErr)}
}

// ReadFlt3VecList reads multiple flt3Vec records
func ReadFlt3VecList(fields []string, finalErr *error) (val [][3]float64) {
	val = make([][3]float64, len(fields))
	for i, field := range fields {
		val[i] = ReadFlt3Vec(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadFlt3VecList failed")
			return val
		}
	}
	return val
}

// Constants required for ReadFltMatrix.
const fltMatrixBegin = "[][]float64{[]float64{"
const fltMatrixEnd = "}}"
const fltMatrixSep = "},[]float64{"

// ReadFltMatrix takes a string representation of a [][]float64 literal and returns a type [][]float64.
func ReadFltMatrix(str string, finalErr *error) [][]float64 {
	str = strings.Replace(str, " ", "", -1)
	if strings.Count(str, fltMatrixBegin) != 1 {
		*finalErr = errcat.AppendErrStr(*finalErr, "Beginning of matrix not formatted as: "+fltMatrixBegin)
		return nil
	}
	if strings.Count(str, fltMatrixEnd) != 1 {
		*finalErr = errcat.AppendErrStr(*finalErr, "End of matrix not formatted as: "+fltMatrixEnd)
		return nil
	}
	if !(strings.Count(str, fltMatrixSep) > 0) {
		*finalErr = errcat.AppendErrStr(*finalErr, "Rows of matrix not seperated by: "+fltMatrixSep)
		return nil
	}
	str = strings.Replace(str, fltMatrixBegin, "", -1)
	str = strings.Replace(str, fltMatrixEnd, "", -1)
	strs := strings.Split(str, fltMatrixSep)
	out := make([][]float64, len(strs))
	for j, astr := range strs {
		inStrs := strings.Split(astr, ",")
		out[j] = make([]float64, len(inStrs))
		for i, inStr := range inStrs {
			out[j][i] = ReadFlt(inStr, finalErr)
		}
	}
	return out
}

// ReadFltMatrixList reads multiple flt matrix fields
func ReadFltMatrixList(fields []string, finalErr *error) (val [][][]float64) {
	val = make([][][]float64, len(fields))
	for i, field := range fields {
		val[i] = ReadFltMatrix(field, finalErr)
		if errcat.IsErr(*finalErr) {
			*finalErr = errcat.AppendErrStr(*finalErr, "ReadFltMatrixList failed")
			return val
		}
	}
	return val
}

// ReadCommaStr converts multiple fields into a string with commas
func ReadCommaStr(fields []string, finalErr *error) (val string) {
	return ReadStr(strings.Join(fields, ", "), finalErr)
}

// NumRecFields returns the appropriate error message when given the name of a record type an equality string and the expected value
func NumRecFields(label string, rec []string, equality string, expected int) (err error) {
	actual := len(rec)
	switch equality {
	case "!=":
		if actual != expected {
			return fmt.Errorf("%s record has %d fields expected %d, %#v", label, actual, expected, rec)
		}
	case ">=":
		if actual >= expected {
			return fmt.Errorf("%s record has %d fields expected at most %d, %#v", label, actual, expected, rec)
		}
	case "<=":
		if actual <= expected {
			return fmt.Errorf("%s record has %d fields expected at least %d, %#v", label, actual, expected, rec)
		}
	case ">":
		if actual > expected {
			return fmt.Errorf("%s record has %d fields expected at most %d, %#v", label, actual, expected, rec)
		}
	case "<":
		if actual < expected {
			return fmt.Errorf("%s record has %d fields expected at least %d, %#v", label, actual, expected, rec)
		}
	case "==":
		if actual == expected {
			return fmt.Errorf("%s record has %d fields expected anything but %d, %#v", label, actual, expected, rec)
		}
	}
	return nil
}

// GetPercentMinMax returns the value percent of the way from min(minmax[0]) to max(minmax[1])
func GetPercentMinMax(minmax [2]float64, percent float64) (val float64) {
	val = minmax[0] + ((minmax[1] - minmax[0]) * (percent / 100.0))
	return val
}
