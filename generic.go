/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package csvrec

import (
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/srosencrantz/errcat"
)

// RecordTag is the struct field tag for Records
const RecordTag = "csvrecRec"

// FieldTag is the struct field tag for Fields
const FieldTag = "csvrecField"

// ParseFile is a wrapper around ParseRecMap. ParseFile creates a record map from a csvrec formated file and passes it to ParseRecMap
func ParseFile(fileName string, fileStruct interface{}, finalErr *error) (newFile interface{}) {
	recMap, err := ReadFile(fileName)
	if err != nil {
		*finalErr = errcat.AppendErr(*finalErr, err)
		return nil
	}
	newFile = ParseRecMap(recMap, fileStruct, finalErr)
	return newFile
}

// ParseRecMap fills a struct with csvrec tags from a record map
func ParseRecMap(recMap RecordMap, fileStruct interface{}, finalErr *error) (newFile interface{}) {
	fst := reflect.TypeOf(fileStruct)
	fsvp := reflect.New(fst)
	fsv := fsvp.Elem()

	if fsv.Kind() != reflect.Struct {
		*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("fileStruct is not a struct, but is a %s", fsv.Kind().String()))
		return fsv.Interface()
	}

	for i := 0; i < fsv.NumField(); i++ {
		tag, ok := fst.Field(i).Tag.Lookup(RecordTag)
		if !ok {
			continue
		}

		if fsv.Field(i).Kind() != reflect.Slice {
			*finalErr = errcat.AppendErr(*finalErr, fmt.Errorf("recList is not a slice: %s, %s", fsv.Field(i).Kind().String(), fsv.Field(i)))
			return fsv.Interface()
		}
		newVal, err := getRec(recMap, tag, reflect.MakeSlice(fst.Field(i).Type, 0, 0).Interface())
		if err != nil {
			*finalErr = errcat.AppendErr(*finalErr, err)
			return fsv.Interface()
		}
		fsv.Field(i).Set(reflect.ValueOf(newVal))

	}
	return fsv.Interface()
}

// CreateFile fills a struct with csvrec tags from a csvrec formated file
func CreateFile(fileName, suffix string, fileStruct interface{}) (err error) {
	newMap := make(RecordMap)
	fieldOrder := make([]string, 0)

	fsv := reflect.ValueOf(fileStruct)
	fst := reflect.TypeOf(fileStruct)
	if fsv.Kind() != reflect.Struct {
		return fmt.Errorf("fileStruct is not a struct, but is a %s", fsv.Kind().String())
	}

	for i := 0; i < fsv.NumField(); i++ {
		tag, ok := fst.Field(i).Tag.Lookup(RecordTag)
		if !ok {
			continue
		}

		if fsv.Field(i).Kind() != reflect.Slice {
			return fmt.Errorf("recList is not a slice: %s, %s", fsv.Field(i).Kind().String(), fsv.Field(i))
		}

		err := putRec(newMap, tag, fsv.Field(i).Interface())
		if err != nil {
			return err
		}
		fieldOrder = append(fieldOrder, tag)
	}

	if !strings.HasSuffix(fileName, suffix) {
		fileName += suffix
	}

	err = WriteSimpleFile(fileName, newMap, fieldOrder)
	return err
}

func getRec(recMap RecordMap, recName string, recList interface{}) (newList interface{}, err error) {
	list := reflect.ValueOf(recList)
	elemType := reflect.TypeOf(list.Interface()).Elem().Elem()

	// get the number of fields in the record
	tmpRecPtr := reflect.New(elemType)
	tmpRec := reflect.Indirect(tmpRecPtr)
	tags := []string{}
	fieldIndx := []int{}

	for i := 0; i < tmpRec.NumField(); i++ {
		tag, ok := elemType.Field(i).Tag.Lookup(FieldTag)
		if ok {
			tags = append(tags, strings.TrimSpace(strings.Split(tag, ",")[0]))
			fieldIndx = append(fieldIndx, i)
		}
	}
	numFields := len(tags)

	// Check that repeating and cstr fields are only the last field
	for _, tag := range tags[:len(tags)-1] {
		if strings.Contains(tag, "list") || strings.Contains(tag, "cstr") {
			return nil, fmt.Errorf("field in %s struct has tag %s but is not the last readable field in the struct", recName, tag)
		}
	}

	// don't require lists (that could be nil)
	numListFields := 0
	if strings.Contains(tags[len(tags)-1], "list") || strings.Contains(tags[len(tags)-1], "cstr") {
		numListFields++
	}

	// read in the records
	for _, rec := range recMap[recName] {

		if err := NumRecFields(recName, rec, "<", numFields+1-numListFields); err != nil { // +1 is for record label
			return nil, err
		}

		newRecPtr := reflect.New(elemType)
		newRec := reflect.Indirect(newRecPtr)
		for i := 0; i < numFields; i++ {
			strIdx := i + 1
			defer func() {
				if r := recover(); r != nil {
					fmt.Println(".Set panic", recName, tags[i], rec, i, rec[strIdx])
				}
			}()

			var newVal reflect.Value
			switch tags[i] {
			case "int":
				newVal = reflect.ValueOf(ReadInt(rec[strIdx], &err))
			case "flt":
				newVal = reflect.ValueOf(ReadFlt(rec[strIdx], &err))
			case "bool":
				newVal = reflect.ValueOf(ReadBool(rec[strIdx], &err))
			case "fpair":
				newVal = reflect.ValueOf(ReadFltPair(rec[strIdx], &err))
			case "frange":
				newVal = reflect.ValueOf(ReadFltRange(rec[strIdx], &err))
			case "f2vec":
				newVal = reflect.ValueOf(ReadFlt2Vec(rec[strIdx], &err))
			case "f3vec":
				newVal = reflect.ValueOf(ReadFlt3Vec(rec[strIdx], &err))
			case "ipair":
				newVal = reflect.ValueOf(ReadIntPair(rec[strIdx], &err))
			case "irange":
				newVal = reflect.ValueOf(ReadIntRange(rec[strIdx], &err))
			case "i2vec":
				newVal = reflect.ValueOf(ReadInt2Vec(rec[strIdx], &err))
			case "i3vec":
				newVal = reflect.ValueOf(ReadInt3Vec(rec[strIdx], &err))
			case "fmatrix":
				newVal = reflect.ValueOf(ReadFltMatrix(rec[strIdx], &err))
			case "str":
				newVal = reflect.ValueOf(strings.TrimSpace(rec[strIdx]))
			case "cstr":
				newVal = reflect.ValueOf(ReadCommaStr(rec[strIdx:], &err))
			case "intlist":
				newVal = reflect.ValueOf(ReadIntList(rec[strIdx:], &err))
			case "fltlist":
				newVal = reflect.ValueOf(ReadFltList(rec[strIdx:], &err))
			case "boollist":
				newVal = reflect.ValueOf(ReadBoolList(rec[strIdx:], &err))
			case "fpairlist":
				newVal = reflect.ValueOf(ReadFltPairList(rec[strIdx:], &err))
			case "frangelist":
				newVal = reflect.ValueOf(ReadFltRangeList(rec[strIdx:], &err))
			case "f2veclist":
				newVal = reflect.ValueOf(ReadFlt2VecList(rec[strIdx:], &err))
			case "f3veclist":
				newVal = reflect.ValueOf(ReadFlt3VecList(rec[strIdx:], &err))
			case "ipairlist":
				newVal = reflect.ValueOf(ReadIntPairList(rec[strIdx:], &err))
			case "irangelist":
				newVal = reflect.ValueOf(ReadIntRangeList(rec[strIdx:], &err))
			case "i2veclist":
				newVal = reflect.ValueOf(ReadInt2VecList(rec[strIdx:], &err))
			case "i3veclist":
				newVal = reflect.ValueOf(ReadInt3VecList(rec[strIdx:], &err))
			case "fmatrixlist":
				newVal = reflect.ValueOf(ReadFltMatrixList(rec[strIdx:], &err))
			case "strlist":
				newVal = reflect.ValueOf(TrimList(rec[strIdx:]))
			}
			if err != nil {
				return nil, err
			}
			newRec.Field(fieldIndx[i]).Set(newVal)

			if strings.Contains(tags[i], "list") || strings.Contains(tags[i], "cstr") {
				break
			}
		}
		list = reflect.Append(list, newRecPtr)
	}

	return list.Interface(), nil
}

func putRec(recMap RecordMap, recName string, recList interface{}) (err error) {
	list := reflect.ValueOf(recList)
	elemType := reflect.TypeOf(list.Interface()).Elem().Elem()
	if list.Len() == 0 {
		return nil
	}
	// Create the comment line
	tags := []string{}
	fieldIndx := []int{}
	newRec := []string{"// " + recName}
	for i := 0; i < elemType.NumField(); i++ {
		tagstr, ok := elemType.Field(i).Tag.Lookup(FieldTag)
		if ok {
			taglist := strings.Split(tagstr, ",")
			label := elemType.Field(i).Name
			if len(taglist) > 1 {
				label = strings.TrimSpace(taglist[1])
			}
			newRec = append(newRec, label)
			tags = append(tags, strings.TrimSpace(taglist[0]))
			fieldIndx = append(fieldIndx, i)
		}
	}
	recMap[recName] = append(recMap[recName], newRec)
	numFields := len(tags)

	// Write the records to the recmap
	for i := 0; i < list.Len(); i++ {
		newRec := []string{recName}
	fieldloop:
		for j := 0; j < numFields; j++ {
			var fieldStr string
			var fieldStrs []string
			field := reflect.Indirect(list.Index(i)).Field(fieldIndx[j]).Interface()
			switch tags[j] {
			case "int":
				fieldStr = Int2Str(field.(int))
			case "flt":
				fieldStr = Flt2Str(field.(float64))
			case "bool":
				fieldStr = Bool2StrB(field.(bool))
			case "fpair":
				fieldStr = FltPair2Str(field.([2]float64))
			case "frange":
				fieldStr = FltRange2Str(field.([3]float64))
			case "f2vec":
				fieldStr = Flt2Vec2Str(field.([2]float64))
			case "f3vec":
				fieldStr = Flt3Vec2Str(field.([3]float64))
			case "ipair":
				fieldStr = IntPair2Str(field.([2]int))
			case "irange":
				fieldStr = IntRange2Str(field.([3]int))
			case "i2vec":
				fieldStr = Int2Vec2Str(field.([2]int))
			case "i3vec":
				fieldStr = Int3Vec2Str(field.([3]int))
			case "fmatrix":
				fieldStr = FltMatrix2Str(field.([][]float64))
			case "str":
				fieldStr = field.(string)
			case "cstr":
				fieldStr = field.(string)
			case "intlist":
				fieldStrs = IntList2Str(field.([]int))
			case "fltlist":
				fieldStrs = FltList2Str(field.([]float64))
			case "boollist":
				fieldStrs = BoolList2Str(field.([]bool))
			case "fpairlist":
				fieldStrs = FltPairList2Str(field.([][2]float64))
			case "frangelist":
				fieldStrs = FltRangeList2Str(field.([][3]float64))
			case "f2veclist":
				fieldStrs = Flt2VecList2Str(field.([][2]float64))
			case "f3veclist":
				fieldStrs = Flt3VecList2Str(field.([][3]float64))
			case "ipairlist":
				fieldStrs = IntPairList2Str(field.([][2]int))
			case "irangelist":
				fieldStrs = IntRangeList2Str(field.([][3]int))
			case "i2veclist":
				fieldStrs = Int2VecList2Str(field.([][2]int))
			case "i3veclist":
				fieldStrs = Int3VecList2Str(field.([][3]int))
			case "fmatrixlist":
				fieldStrs = FltMatrixList2Str(field.([][][]float64))
			case "strlist":
				fieldStrs = field.([]string)
			}
			if strings.Contains(tags[j], "list") {
				newRec = append(newRec, fieldStrs...)
				break fieldloop
			} else {
				newRec = append(newRec, fieldStr)
			}
		}
		recMap[recName] = append(recMap[recName], newRec)
	}

	return nil
}
