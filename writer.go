/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package csvrec

import (
	"fmt"
	"os"
	"strings"

	"bitbucket.org/srosencrantz/csv"
)

// WriteFile writes a csvrecord file given a map of record lists
func WriteFile(name string, recMap RecordMap) (err error) {
	var f *os.File
	if f, err = os.Create(name); err != nil {
		return err
	}
	defer f.Close()

	wcsv := csv.NewWriter(f)
	for _, records := range recMap {
		PadRecords(records)
		wcsv.WriteAll(records)
		if err = wcsv.Error(); err != nil {
			return err
		}
	}
	return nil
}

// WriteOrderedFile writes a csvrecord file, one record type at a time in the order given
func WriteOrderedFile(name string, recMap RecordMap, recNames []string) (err error) {
	var f *os.File
	if f, err = os.Create(name); err != nil {
		return err
	}
	defer f.Close()

	wcsv := csv.NewWriter(f)
	for _, rec := range recNames {
		records := recMap[rec]
		PadRecords(records)
		wcsv.WriteAll(records)
		if err = wcsv.Error(); err != nil {
			return err
		}
	}
	return nil

}

// WriteSimpleFile writes a csvrecord file, one record type at a time in the order given, with no padding except for a one space after the comma
func WriteSimpleFile(name string, recMap RecordMap, recNames []string) (err error) {
	var f *os.File
	if f, err = os.Create(name); err != nil {
		return err
	}
	defer f.Close()
	blankRecord := [][]string{[]string{""}}
	wcsv := csv.NewWriter(f)
	for _, recName := range recNames {
		records := recMap[recName]
		if len(records) == 0 {
			continue
		}
		for _, rec := range records {
			newrec := [][]string{Dup(rec)}
			prePadRecords(newrec)
			wcsv.WriteAll(newrec)
		}
		wcsv.WriteAll(blankRecord)
		if err = wcsv.Error(); err != nil {
			return err
		}
	}
	return nil

}

// WriteCsvRecFile writes a csvrecord file, one record type at a time in the order given with or without padding each section.
func WriteCsvRecFile(name string, recMap RecordMap, recNames []string, padded []bool) (err error) {
	if len(recNames) != len(padded) {
		return fmt.Errorf("WriteCsvRecFile was passed recNames length: %d and padded length: %d, they should be equal", len(recNames), len(padded))
	}

	var f *os.File
	if f, err = os.Create(name); err != nil {
		return err
	}
	defer f.Close()

	wcsv := csv.NewWriter(f)
	for i, rec := range recNames {
		records := recMap[rec]
		if padded[i] {
			PadRecords(records)
		}
		wcsv.WriteAll(records)
		if err = wcsv.Error(); err != nil {
			return err
		}
	}
	return nil

}

// The following are helper functions

//prePadRecords adds spaces so that commas will have one space after them, it adds a space to the begining of each record that is not in the [:][0] column
func prePadRecords(records [][]string) {
	for i := range records {
		for j := range records[i] {
			if j != 0 {
				records[i][j] = fmt.Sprintf(" %s", records[i][j])
			}
		}
	}
}

// PadRecords pads the strings in a [][]string so that the columns match
func PadRecords(records [][]string) {
	// determine the max # of columns (each row could be different
	maxCol := 0
	for _, rec := range records {
		if len(rec) > maxCol {
			maxCol = len(rec)
		}
	}
	// for each column find the longest string (maxField) then pad every string in the column to be maxField+1
	for i := 0; i < maxCol; i++ {
		maxField := 0
		for _, rec := range records {
			if len(rec) > i {
				if len(rec[i]) > maxField {
					maxField = len(rec[i])
				}
			}
		}
		for j, rec := range records {
			if len(rec) > i {
				padLen := maxField + 1
				fmtStr := fmt.Sprintf("%ds", padLen)
				if j == 0 || i == 0 {
					fmtStr = "%-" + fmtStr
				} else {
					fmtStr = "%" + fmtStr
				}
				rec[i] = fmt.Sprintf(fmtStr, rec[i])
			}
		}
	}
}

// Int2Str converts and int to a trimed string with a consistant format
func Int2Str(num int) string {
	return strings.TrimSpace(fmt.Sprintf("%d", num))
}

// IntList2Str converts a list of ints to a list of strings
func IntList2Str(vals []int) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = Int2Str(val)
	}
	return strs
}

// Flt2Str converts a float64 to a trimed string with a consistant format
func Flt2Str(num float64) string {
	return strings.TrimSpace(fmt.Sprintf("%12.8e", num))
}

// FltList2Str converts a list of flts to a list of strings
func FltList2Str(vals []float64) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = Flt2Str(val)
	}
	return strs
}

// Flt2StrPrec converts a float64 to a trimed string with a consistant format
func Flt2StrPrec(num float64, format string) string {
	return strings.TrimSpace(fmt.Sprintf(format, num))
}

// Bool2Str converts a boolean to a trimed string (containing a 1 or a 0) with a consistant format
func Bool2Str(flag bool) string {
	temp := 0
	if flag {
		temp = 1
	}
	return Int2Str(temp)
}

// Bool2StrB converts a boolean to a trimed string (containing true or false) with a consistant format
func Bool2StrB(flag bool) string {
	temp := "false"
	if flag {
		temp = "true"
	}
	return temp
}

// BoolList2Str converts a list of bools to a list of strings
func BoolList2Str(vals []bool) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = Bool2StrB(val)
	}
	return strs
}

// FltPair2Str converts a float pair to a colon seperated string
func FltPair2Str(pair [2]float64) string {
	if pair[0] == pair[1] {
		return Flt2Str(pair[0])
	}
	return strings.TrimSpace(fmt.Sprintf("%s:%s", Flt2Str(pair[0]), Flt2Str(pair[1])))
}

// FltPairList2Str converts a list of fltpairs to a list of strings
func FltPairList2Str(vals [][2]float64) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = FltPair2Str(val)
	}
	return strs
}

// FltRange2Str converts a float pair to a colon seperated string
func FltRange2Str(rng [3]float64) string {
	if rng[0] == rng[1] && rng[0] == rng[2] {
		return Flt2Str(rng[0])
	}
	return strings.TrimSpace(fmt.Sprintf("%s:%s:%s", Flt2Str(rng[0]), Flt2Str(rng[1]), Flt2Str(rng[2])))
}

// FltRangeList2Str converts a list of fltranges to a list of strings
func FltRangeList2Str(vals [][3]float64) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = FltRange2Str(val)
	}
	return strs
}

// Flt2Vec2Str converts a [2]float64 array to a colon seperated string
func Flt2Vec2Str(vec [2]float64) string {
	return strings.TrimSpace(fmt.Sprintf("%s:%s", Flt2Str(vec[0]), Flt2Str(vec[1])))
}

// Flt2VecList2Str converts a list of flt2vecs to a list of strings
func Flt2VecList2Str(vals [][2]float64) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = Flt2Vec2Str(val)
	}
	return strs
}

// Flt3Vec2Str converts a [3]float64 array to a colon seperated string
func Flt3Vec2Str(vec [3]float64) string {
	return strings.TrimSpace(fmt.Sprintf("%s:%s:%s", Flt2Str(vec[0]), Flt2Str(vec[1]), Flt2Str(vec[2])))
}

// Flt3VecList2Str converts a list of flt3Vecs to a list of strings
func Flt3VecList2Str(vals [][3]float64) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = Flt3Vec2Str(val)
	}
	return strs
}

// IntPair2Str converts a float pair to a colon seperated string
func IntPair2Str(pair [2]int) string {
	if pair[0] == pair[1] {
		return Int2Str(pair[0])
	}
	return strings.TrimSpace(fmt.Sprintf("%s:%s", Int2Str(pair[0]), Int2Str(pair[1])))
}

// IntPairList2Str converts a list of intpairs to a list of strings
func IntPairList2Str(vals [][2]int) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = IntPair2Str(val)
	}
	return strs
}

// IntRange2Str converts a Integer Range to a colon seperated string of 3 integers unless they're all the same and then you get 1 integer
func IntRange2Str(rng [3]int) string {
	if rng[0] == rng[1] && rng[0] == rng[2] {
		return Int2Str(rng[0])
	}
	return strings.TrimSpace(fmt.Sprintf("%s:%s:%s", Int2Str(rng[0]), Int2Str(rng[1]), Int2Str(rng[2])))
}

// IntRangeList2Str converts a list of intranges to a list of strings
func IntRangeList2Str(vals [][3]int) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = IntRange2Str(val)
	}
	return strs
}

// Int2Vec2Str converts a [2]int array to a colon seperated string
func Int2Vec2Str(vec [2]int) string {
	return strings.TrimSpace(fmt.Sprintf("%s:%s", Int2Str(vec[0]), Int2Str(vec[1])))
}

// Int2VecList2Str converts a list of int2vecs to a list of strings
func Int2VecList2Str(vals [][2]int) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = Int2Vec2Str(val)
	}
	return strs
}

// Int3Vec2Str converts a [3]int array to a colon seperated string
func Int3Vec2Str(vec [3]int) string {
	return strings.TrimSpace(fmt.Sprintf("%s:%s:%s", Int2Str(vec[0]), Int2Str(vec[1]), Int2Str(vec[2])))
}

// Int3VecList2Str converts a list of int3Vecs to a list of strings
func Int3VecList2Str(vals [][3]int) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = Int3Vec2Str(val)
	}
	return strs
}

// FltMatrix2Str writes a [][]float64 as a Golang literal string.
func FltMatrix2Str(fltmat [][]float64) string {

	return fmt.Sprintf("%#v", fltmat)

}

// FltMatrixList2Str converts a list of floatMatrixs to a list of strings
func FltMatrixList2Str(vals [][][]float64) (strs []string) {
	strs = make([]string, len(vals))
	for i, val := range vals {
		strs[i] = FltMatrix2Str(val)
	}
	return strs
}

//Dup creates a copy of a record
func Dup(record []string) (new []string) {
	new = make([]string, len(record))
	copy(new, record)
	return new
}
