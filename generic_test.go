/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package csvrec

import (
	"os"
	"testing"

	"bitbucket.org/srosencrantz/testhelp"
)

// example file type description
type TestFile struct {
	TestRecAs []*TestRecA `csvrecRec:"TestRecA"`
	TestRecBs []*TestRecB `csvrecRec:"TestRecB"`
	OtherData []float64   // these should not be read or written
}

// example record type description
type TestRecA struct {
	Xint  int        `csvrecField:"int"`
	Xflt  float64    `csvrecField:"flt"`
	Xbool bool       `csvrecField:"bool"`
	Fpair [2]float64 `csvrecField:"fpair"`
	//	FpairList [][2]float64 `csvrecField:"fpairlist"` // used this to test that the list location checking was working
	Frange   [3]float64 `csvrecField:"frange"`
	F2Vec    [2]float64 `csvrecField:"f2vec"`
	F3Vec    [3]float64 `csvrecField:"f3vec"`
	otherint int        // this field shouldn't be read or written
	XintList []int      `csvrecField:"intlist"`
}

type TestRecB struct {
	Otherflt  float64     // this field shouldn't be read or written
	Ipair     [2]int      `csvrecField:"ipair"`
	Irange    [3]int      `csvrecField:"irange"`
	I2Vec     [2]int      `csvrecField:"i2vec"`
	Otherflt2 float64     // this field shouldn't be read or written
	I3Vec     [3]int      `csvrecField:"i3vec"`
	FltMatrix [][]float64 `csvrecField:"fmatrix"`
	Str       string      `csvrecField:"str"`
	CStr      string      `csvrecField:"cstr"`
}

func CompTestRecA(t *testing.T, a, b *TestRecA) {
	testhelp.IntTest(t, "TestRecA Xint", a.Xint, b.Xint)
	testhelp.FloatTest(t, "TestRecA Xflt", a.Xflt, b.Xflt)
	testhelp.BoolTest(t, "TestRecA Xbool", a.Xbool, b.Xbool)
	testhelp.Float2VecTest(t, "TestRecA Fpair", a.Fpair, b.Fpair)
	testhelp.Float3VecTest(t, "TestRecA Frange", a.Frange, b.Frange)
	testhelp.Float2VecTest(t, "TestRecA F2Vec", a.F2Vec, b.F2Vec)
	testhelp.Float3VecTest(t, "TestRecA F3Vec", a.F3Vec, b.F3Vec)
	testhelp.IntVecTest(t, "TestRecA intlist", a.XintList, b.XintList)
}

func CompTestRecB(t *testing.T, a, b *TestRecB) {
	testhelp.Int2VecTest(t, "TestRecB Ipair", a.Ipair, b.Ipair)
	testhelp.Int3VecTest(t, "TestRecB Irange", a.Irange, b.Irange)
	testhelp.Int2VecTest(t, "TestRecB I2Vec", a.I2Vec, b.I2Vec)
	testhelp.Int3VecTest(t, "TestRecB I3Vec", a.I3Vec, b.I3Vec)
	testhelp.FloatMatrixTest(t, "TestRecB FltMatrix", a.FltMatrix, b.FltMatrix)
	testhelp.StringTest(t, "TestRecB Str", a.Str, b.Str)
	testhelp.StringTest(t, "TestRecB CStr", a.CStr, b.CStr)
}

// TestCsvRec tests the csvrec library
func TestGenericIO(t *testing.T) {
	var err error
	fileStruct := ParseFile("csvSample2", TestFile{}, &err).(TestFile)
	if err != nil {
		t.Fatalf("ParseFile returned an error: %s", err)
	}

	// check the number of TestRecAs and Bs
	if len(fileStruct.TestRecAs) != 2 {
		t.Errorf("expected %d fileStruct.TestRecAs got %d", 2, len(fileStruct.TestRecAs))
	}
	if len(fileStruct.TestRecBs) != 2 {
		t.Errorf("expected %d fileStruct.TestRecBs got %d", 2, len(fileStruct.TestRecBs))
	}

	// add some records
	newRecA := TestRecA{
		Xint:   9,
		Xflt:   9.8,
		Xbool:  false,
		Fpair:  [2]float64{0.0, 1.0},
		Frange: [3]float64{3.2, 1.0, 9.8},
		F2Vec:  [2]float64{0.0, 1.0},
		F3Vec:  [3]float64{3.2, 1.0, 9.8}}

	newRecB := TestRecB{
		Ipair:     [2]int{1, 2},
		Irange:    [3]int{3, 2, 10},
		I2Vec:     [2]int{1, 2},
		I3Vec:     [3]int{3, 2, 10},
		FltMatrix: [][]float64{[]float64{5.6, 7.8, 8.9}, []float64{1.2, 3.4, 5.6}},
		Str:       "This is a test",
		CStr:      "This is also a test, but with commas!,,,,"}

	fileStruct.TestRecAs = append(fileStruct.TestRecAs, &newRecA)
	fileStruct.TestRecBs = append(fileStruct.TestRecBs, &newRecB)

	// write the modified fileStruct to a file
	err = CreateFile("csvSample2Temp", ".tmp", fileStruct)
	if err != nil {
		t.Errorf("CreateFile returned an error: %s", err)
	}

	// read the new file back in
	fileStruct2 := ParseFile("csvSample2Temp.tmp", TestFile{}, &err).(TestFile)
	if err != nil {
		t.Fatalf("ParseFile csvSample2Temp returned an error: %s", err)
	}

	// check the number of TestRecAs and Bs
	if len(fileStruct2.TestRecAs) != len(fileStruct.TestRecAs) {
		t.Fatalf("expected %d fileStruct2.TestRecAs got %d", len(fileStruct.TestRecAs), len(fileStruct2.TestRecAs))
	}
	if len(fileStruct2.TestRecBs) != len(fileStruct.TestRecBs) {
		t.Fatalf("expected %d fileStruct2.TestRecBs got %d", len(fileStruct.TestRecBs), len(fileStruct2.TestRecBs))
	}

	// compare all the TestRecAs and Bs
	for i := range fileStruct.TestRecAs {
		CompTestRecA(t, fileStruct.TestRecAs[i], fileStruct2.TestRecAs[i])
	}
	for i := range fileStruct.TestRecBs {
		CompTestRecB(t, fileStruct.TestRecBs[i], fileStruct2.TestRecBs[i])
	}

	err = os.Remove("csvSample2Temp.tmp")
	if err != nil {
		t.Errorf("os.Remove returned an error: %s", err)
	}
}
