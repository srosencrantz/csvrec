/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package csvrec

import (
	"os"
	"testing"

	"bitbucket.org/srosencrantz/testhelp"
)

// example record type description
type TestRec struct {
	Xint      int
	Xflt      float64
	Xbool     bool
	Fpair     [2]float64
	Frange    [3]float64
	F2Vec     [2]float64
	F3Vec     [3]float64
	Ipair     [2]int
	Irange    [3]int
	I2Vec     [2]int
	I3Vec     [3]int
	FltMatrix [][]float64
	Str       string
	CStr      string
}

// TestRecLabel is the label of the TestRec record
const TestRecLabel string = "TestRec"

// TestRecFields is the number of external fields in the TestRec struct
const TestRecFields int = 10

// TestRecComment is the list of strings used for the TestRecComment
var TestRecComment = []string{"// " + TestRecLabel, "xint", "xflt", "xbool", "fpair", "frange", "ipair", "irange", "str", "cstr", "FltMatrix"}

type TestRecList []*TestRec

func GetTestRecs(recMap RecordMap) (trl TestRecList, err error) {
	records := recMap["TestRec"]
	trl = make(TestRecList, 0)
	for _, rec := range records {
		if err = NumRecFields(TestRecLabel, rec, "<", TestRecFields+1); err != nil {
			return trl, err
		}

		newrec := TestRec{
			Xint:      ReadInt(rec[1], &err),
			Xflt:      ReadFlt(rec[2], &err),
			Xbool:     ReadBool(rec[3], &err),
			Fpair:     ReadFltPair(rec[4], &err),
			Frange:    ReadFltRange(rec[5], &err),
			F2Vec:     ReadFlt2Vec(rec[6], &err),
			F3Vec:     ReadFlt3Vec(rec[7], &err),
			Ipair:     ReadIntPair(rec[8], &err),
			Irange:    ReadIntRange(rec[9], &err),
			I2Vec:     ReadInt2Vec(rec[10], &err),
			I3Vec:     ReadInt3Vec(rec[11], &err),
			FltMatrix: ReadFltMatrix(rec[12], &err),
			Str:       ReadStr(rec[13], &err),
			CStr:      ReadCommaStr(rec[14:], &err)}

		trl = append(trl, &newrec)
	}
	return trl, err
}

func PutTestRecs(recMap RecordMap, trl TestRecList) {
	if trl == nil {
		return
	}
	recMap[TestRecLabel] = append(recMap[TestRecLabel], TestRecComment)
	for _, s := range trl {
		newRecStrs := []string{
			TestRecLabel,
			Int2Str(s.Xint),
			Flt2Str(s.Xflt),
			Bool2Str(s.Xbool),
			FltPair2Str(s.Fpair),
			FltRange2Str(s.Frange),
			Flt2Vec2Str(s.F2Vec),
			Flt3Vec2Str(s.F3Vec),
			IntPair2Str(s.Ipair),
			IntRange2Str(s.Irange),
			Int2Vec2Str(s.I2Vec),
			Int3Vec2Str(s.I3Vec),
			FltMatrix2Str(s.FltMatrix),
			s.Str,
			s.CStr,
		}
		recMap[TestRecLabel] = append(recMap[TestRecLabel], newRecStrs)
	}
}

func CompTestRec(t *testing.T, a, b *TestRec) {
	testhelp.IntTest(t, "TestRec Xint", a.Xint, b.Xint)
	testhelp.FloatTest(t, "TestRec Xflt", a.Xflt, b.Xflt)
	testhelp.BoolTest(t, "TestRec Xbool", a.Xbool, b.Xbool)
	testhelp.Float2VecTest(t, "TestRec Fpair", a.Fpair, b.Fpair)
	testhelp.Float3VecTest(t, "TestRec Frange", a.Frange, b.Frange)
	testhelp.Float2VecTest(t, "TestRec F2Vec", a.F2Vec, b.F2Vec)
	testhelp.Float3VecTest(t, "TestRec F3Vec", a.F3Vec, b.F3Vec)
	testhelp.Int2VecTest(t, "TestRec Ipair", a.Ipair, b.Ipair)
	testhelp.Int3VecTest(t, "TestRec Irange", a.Irange, b.Irange)
	testhelp.Int2VecTest(t, "TestRec I2Vec", a.I2Vec, b.I2Vec)
	testhelp.Int3VecTest(t, "TestRec I3Vec", a.I3Vec, b.I3Vec)
	testhelp.FloatMatrixTest(t, "TestRec FltMatrix", a.FltMatrix, b.FltMatrix)
	testhelp.StringTest(t, "TestRec Str", a.Str, b.Str)
	testhelp.StringTest(t, "TestRec CStr", a.CStr, b.CStr)
}

// TestCsvRec tests the csvrec library
func TestCsvRecIO(t *testing.T) {

	recMap, err3 := ReadFile("csvSample")
	if err3 != nil {
		t.Errorf("Read 1 returned an error: %s", err3)
	}

	bll, err := GetTestRecs(recMap)
	if err != nil {
		t.Errorf("GetTestRecs 1 returned an error: %s", err)
	}

	// check the number of TestRecs
	if len(bll) != 2 {
		t.Errorf("expected 2 got %1d", len(bll))
	}

	// add another record
	newRec := TestRec{
		Xint:      9,
		Xflt:      9.8,
		Xbool:     false,
		Fpair:     [2]float64{0.0, 1.0},
		Frange:    [3]float64{3.2, 1.0, 9.8},
		F2Vec:     [2]float64{0.0, 1.0},
		F3Vec:     [3]float64{3.2, 1.0, 9.8},
		Ipair:     [2]int{1, 2},
		Irange:    [3]int{3, 2, 10},
		I2Vec:     [2]int{1, 2},
		I3Vec:     [3]int{3, 2, 10},
		FltMatrix: [][]float64{[]float64{5.6, 7.8, 8.9}, []float64{1.2, 3.4, 5.6}},
		Str:       "This is a test",
		CStr:      "This is also a test, but with commas!,,,,"}

	bll = append(bll, &newRec)

	// put the modified bll into a new map and write it out
	newMap := make(RecordMap)
	PutTestRecs(newMap, bll)
	err = WriteFile("csvWriteSample", newMap)
	if err != nil {
		t.Errorf("Write returned an error: %s", err)
	}

	// read the new file back in
	recMap1, err4 := ReadFile("csvWriteSample")
	if err4 != nil {
		t.Errorf("Read 1 returned an error: %s", err4)
	}

	bll2, err2 := GetTestRecs(recMap1)
	if err2 != nil {
		t.Errorf("GetTestRecs 2 returned an error: %s", err2)
	}

	// check the number of TestRecs
	if len(bll2) != 3 {
		t.Errorf("expected 3 got %1d", len(bll2))
	}

	// compare all the records
	for i := range bll {
		CompTestRec(t, bll[i], bll2[i])
	}

	err6 := os.Remove("csvWriteSample")
	if err6 != nil {
		t.Errorf("os.Remove returned an error: %s", err6)
	}
}

// TestCsvRec tests the csvrec library
func TestSingleValRanges(t *testing.T) {
	recMap, err3 := ReadFile("csvSample")
	if err3 != nil {
		t.Errorf("Read 1 returned an error: %s", err3)
	}

	bll, err := GetTestRecs(recMap)
	if err != nil {
		t.Errorf("GetTestRecs returned an error: %s", err)
	}

	// check the number of TestRecs
	if len(bll) != 2 {
		t.Errorf("expected 2 got %1d", len(bll))
	}

	// check the single value floatpair
	expectedFpair := [2]float64{1.2, 1.2}
	if bll[1].Fpair != expectedFpair {
		t.Errorf("expected %v  got %v", expectedFpair, bll[1].Fpair)
	}

	// check the single value floatrange
	expectedFrange := [3]float64{3.4, 3.4, 3.4}
	if bll[1].Frange != expectedFrange {
		t.Errorf("expected %v  got %v", expectedFrange, bll[1].Frange)
	}

	// check the single value intpair
	expectedIpair := [2]int{3, 3}
	if bll[1].Ipair != expectedIpair {
		t.Errorf("expected %v  got %v", expectedIpair, bll[1].Ipair)
	}

	// check the single value intrange
	expectedIrange := [3]int{4, 4, 4}
	if bll[1].Irange != expectedIrange {
		t.Errorf("expected %v  got %v", expectedIrange, bll[1].Irange)
	}

}

// TestCsvRec tests the csvrec library
func TestMinMax(t *testing.T) {
	recMap, err3 := ReadFile("csvSample")
	if err3 != nil {
		t.Errorf("Read 1 returned an error: %s", err3)
	}

	bll, err := GetTestRecs(recMap)
	if err != nil {
		t.Errorf("GetTestRecs 1 returned an error: %s", err)
	}

	// check minmax
	minmaxex := 4.72
	minmax := GetPercentMinMax(bll[0].Fpair, 10.0)
	if minmax != minmaxex {
		t.Errorf("expected %v  got %v", minmaxex, minmax)
	}

	minmaxex = 1.2
	minmax = GetPercentMinMax(bll[1].Fpair, 10.0)
	if minmax != minmaxex {
		t.Errorf("expected %v  got %v", minmaxex, minmax)
	}
}
